function displayMultiplicationTable(){
	for (let i = 1; i <=10; i++) {
		let product = 5*i
		console.log(`5 "x" ${i} = ${product}`)
}
}
displayMultiplicationTable()


function removeAandE(str){
	let filteredStr = ""
	for(let i=0;i<str.length;i ++){
		if (str[i].toLowerCase()!== "a" && str[i].toLowerCase()!== "e" ) {
			filteredStr += str[i]
		}
	}
	return filteredStr
}